support files for the Eclipse Embedded Systems Register View
https://sourceforge.net/projects/embsysregview/

V0.2:
- added support for %s and CMSIS-SVD dimElementGroup
- see http://mcuoneclipse.com/2014/06/13/embsys-registers-view-supporting-dimelementgroup-of-cmsis-svd

V0.1:
- support for CMSIS-SVD in general and Freescale SVD files.
- see http://mcuoneclipse.com/2014/05/29/how-to-add-register-details-view-in-eclipse/